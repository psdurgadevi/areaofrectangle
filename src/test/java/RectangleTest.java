import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;


public class RectangleTest {
    @Test
    public   void CheckFiftyForLengthFiveBreathTen() {

        Rectangle rectangle = new Rectangle(5,10);

        float area = rectangle.area();

        assertEquals(50, area);

    }

    @Test
   public void CheckThreePointTwoLengthFivePointTwoBreathSixteenPointSixFour() {

        Rectangle rectangle = new Rectangle(3.2F,5.2F);

        float area = rectangle.area();

        assertEquals(16.64F, area);
    }

    @Test
    public void checkPerimeterThirtyLength6Breathnine() {

        Rectangle rectangle =new Rectangle(6,9);

        float perimeter = rectangle.perimeter();

        assertEquals(30,perimeter);
    }


    @Test
    public void checkPerimeter22Length5Point5Breath5Point5() {

        Rectangle rectangle = new Rectangle(5.5F, 5.5F);

        float perimeter = rectangle.perimeter();

        assertEquals(22, perimeter);
    }



}


