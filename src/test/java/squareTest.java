
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertEquals;

public class squareTest {

    @Test
    public void checkArea25SideFive() {

        Square square = new Square(5);

        float area = square.area();

        assertEquals(25, area);
    }

    @Test
    public void checkAreaNineForSideThree() {

        Square square = new Square(3);

        float area = square.area();

        assertEquals(9, area);
    }

    @Test
    public void checkAreaSixPointTwoForSideTwoPointFive() {

        Square square = new Square(2.5F);

        float area = square.area();

        assertEquals(6.25, area);

    }

    @Test
    public void checkPerimeterTwentyForSideFive() {

        Square square = new Square(5);

        float perimeter = square.perimeter();

        assertEquals(20, perimeter);

    }


    @Test
    public void checkPerimeterTenForSideTwoPointFive() {

        Square square = new Square(2.5F);

        float perimeter = square.perimeter();

        assertEquals(10, perimeter);

    }
}
