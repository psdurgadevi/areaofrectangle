public class Rectangle {


    private final float length;
    private final float breath;

    public Rectangle(float length, float breath) {

        this.length = length;
        this.breath = breath;

    }


    public float area() {

        return length * breath;
    }

    public float perimeter() {

        return (2 * (length + breath));
    }


}